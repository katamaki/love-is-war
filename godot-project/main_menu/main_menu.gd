extends Control

@onready var options = %OptionsMenu
@onready var main = %MenuContainer

func _ready():
	%Background.texture = GameInfo.menu_current_background
	options.back.connect(settings_callback)

## BUTTON PRESSES ##

## New Game Button
func _on_new_game_pressed():
	get_tree().change_scene_to_file("res://visual_novel/Introduction.tscn")

## Continue Button
func _on_continue_pressed():
	GameInfo.load_game()

## Options Button
func _on_options_pressed():
	change_settings()

## Quit Button
func _on_quit_pressed():
	get_tree().quit()

## OPTIONS MENU ##
func change_settings():
	main.hide()
	options.show()

func settings_callback():
	main.show()
	options.hide()