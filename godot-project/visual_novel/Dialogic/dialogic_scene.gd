extends Node2D

@onready var PauseMenu = %PauseMenu

## CENA DA INTRODUÇÃO DO JOGO
## TEM ESSE NOME PORQUE ACREDITO QUE DÊ PRA REUTILIZAR ESSA MESMA CENA PRA TODOS
## OS CAPÍTULOS, SÓ MUDANDO O TIMELINE QUE É CARREGADO, ESSA ERA A INTENÇÃO COM A VARIÁVEL
## "CHAPTER" DO GAMEINFO, MAS COMO ESTAVA DANDO ALGUNS PROBLEMAS COM O STYLES_SUBSYSTEM,
## EU ACABEI NÃO TESTANDO DEPOIS QUE ARRUMEI, ENTÃO É UMA IMPLEMENTAÇÃO LEGAL DE TENTAR.




## A IDEIA AQUI É QUE SEMPRE QUE ENTRAMOS NA CENA, SE O JOGO ESTIVER CARREGANDO UM SAVE,
## ELE OBVIAMENTE, CARREGA O SAVE, SE NÃO, ELE INICIA O JOGO DO ZERO.
## POR ALGUM MOTIVO AS VARIÁVEIS NÃO ESTÃO SENDO LEVADAS PRO CAPÍTULO 1, ENTÃO POR ENQUANTO
## TEM QUE FICAR SINCRONIZANDO O DIALOGIC.VAR COM O GAMEINFO, MAS PROVAVELMENTE DA PRA MODIFICAR ISSO
## NO SUBSYSTEM_VAR OU ATÉ MESMO NO START_TIMELINE, VALE A PENA CHECAR DEPOIS O QUE ESTÁ ACONTECENDO.

# Called when the node enters the scene tree for the first time.
func _ready():
	GameInfo.quit_game.connect(_reset)
	if GameInfo.loading == false:
		GameInfo.player_name = "Euzinha"
		Dialogic.VAR.player_name = GameInfo.player_name
		GameInfo.chapter = "Introduction"
		start_dialogue('introduction')
	else:
		GameInfo.loading = false
		Dialogic.VAR.player_name = GameInfo.player_name
		Dialogic.timeline_ended.connect(self._on_timeline_ended)
		Dialogic.load_full_state(GameInfo.data)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


## SEMPRE CONECTAR O SINAL DE FIM DA TIMELINE COM ESSA FUNÇÃO PQ AÍ QUANDO CHEGA NELA DÁ PRA ENVIAR PRA OUTRA CENA
## QUE QUISER, EM GERAL NESSE EXEMPLO DEVERIA SER A PARTIDA DE TUTORIAL. A IDEIA DE FAZER TUDO NA MESMA CENA
## COMPLICARIA UM POUCO NISSO AQUI PORQUE TERIA QUE DIFERENCIAR QUAL CENA É A PRÓXIMA, MAS É SÓ FAZER NO FINAL DE
## CADA TIMELINE UMA VARIÁVEL NO GAMEINFO SER ATUALIZADA PRA INDICAR A PRÓXIMA CENA, AÍ ELE MANDA COM BASE NISSO.

func start_dialogue(timeline:String):
	Dialogic.timeline_ended.connect(self._on_timeline_ended)
	Dialogic.start(timeline)
	
func _on_timeline_ended():
	Dialogic.timeline_ended.disconnect(_on_timeline_ended)
	Dialogic.clear(Dialogic.ClearFlags.TIMLEINE_INFO_ONLY)
	get_tree().change_scene_to_file("res://visual_novel/Chapter1.tscn")

func _reset():
	Dialogic.timeline_ended.disconnect(_on_timeline_ended)
	

#func _input(event: InputEvent):
#	# check if a dialog is already running
#	if Dialogic.current_timeline != null:
#		return
#
#	if event is InputEventKey and event.keycode == KEY_ENTER and event.pressed:
#		Dialogic.start('timeline')
#		get_viewport().set_input_as_handled()
