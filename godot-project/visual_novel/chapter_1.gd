extends Node2D

@onready var PauseMenu = %PauseMenu

## A dialogic_scene.gd EXPLICA TUDO DIREITINHO O QUE TA ACONTECENDO AQUI, ESSE É BASICAMENTE UM CLONE DAQUELE SCRIPT
## ENTÃO CHECAR LÁ PRIMEIRO É MELHOR.

# Called when the node enters the scene tree for the first time.
func _ready():
	GameInfo.quit_game.connect(self._reset)
	Dialogic.VAR.player_name = GameInfo.player_name
	if GameInfo.loading == false:
		GameInfo.chapter = "Chapter1"
		start_dialogue('Chapter1')
	else:
		GameInfo.loading = false
		Dialogic.timeline_ended.connect(self._on_timeline_ended)
		Dialogic.load_full_state(GameInfo.data)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func start_dialogue(timeline:String):
	Dialogic.timeline_ended.connect(self._on_timeline_ended)
	Dialogic.start(timeline)

func _reset():
	Dialogic.timeline_ended.disconnect(self._on_timeline_ended)

func _on_timeline_ended():
	get_tree().quit()
