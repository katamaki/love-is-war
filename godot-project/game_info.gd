extends Node2D

## NOTAS IMPORTANTES (ALTERAÇÕES Q É BOM REVISAR CASO O PLUGIN DÊ UPDATE) ##
##
## A LINHA 246 DO DialogicGameHandler.gd PRECISA CONTER O SEGUINTE CÓDIGO:
##
## ## The Style subsystem needs to run first for others to load correctly.
## if has_subsystem('Styles'):
##  	get_subsystem('Styles').load_game_state()
##
## await get_tree().process_frame
##
## for subsystem in get_children():
##  	if subsystem.name == 'Styles':
##  		continue
##
##  	subsystem.load_game_state()
##
## UM TYPO NO ARQUIVO BASE FAZIA COM QUE OS STYLES NÃO FOSSEM CARREGADOS ANTES DOS OUTROS SUBSYSTEMS.
##
## AS FUNÇÕES DE update_music E update_sound DO AUDIO_SUBSYSTEM PRECISAM TER O VOLUME ALTERADO PARA
## QUE O USUÁRIO POSSA CONTROLAR PELAS CONFIGURAÇÕES DO JOGO, ENTÃO ALÉM DE ALTERAR NO options_menu.gd,
## SEMPRE QUE UM AUDIO NOVO É TOCADO, ELE DEVE TER SEU VOLUME CONSERTADO PARA O VOLUME ATUAL DO JOGO.
##
## O AUTOCOLOR DOS PERSONAGENS PAROU DE FUNCIONAR, ENTÃO A COR DO TEXTO DEVERÁ SER ALTERADA MANUALMENTE,
## TALVEZ VALHA A PENA CHECAR O AUTOCOLOR PRA PROCURAR O PROBLEMA, OU VERIFICAR NA ESCRITA DO TEXTO COMO
## ELE É ALTERADO.
##
## A REGEX PARA NOME DE PERSONAGENS NÃO ACEITA CARACTERES ESPECIAIS, O JOGO FUNCIONARIA NA GODOT, MAS AO EXPORTAR
## ELE CRASHA, ENTÃO OU TENTAR MODIFICAR A REGEX DA GODOT OU NÃO DEIXAR O NOME TER CARACTERES ESPECIAIS OU TENTAR
## ALGUM TRATAMENTO DE ERRO PRA IMPEDIR QUE ELE CRASHE E CONTINUE RODANDO NORMALMENTE.
##
## O EDITOR VISUAL DE TIMELINES É BUGADO, RECOMENDO APRENDER A MEXER NA TIMELINE MANUALMENTE. E NUNCA DÊ CTRL+Z NO
## VISUAL.



## O user:// é uma pasta definida no editor da Godot, ela é criada na pasta Roaming do Windows,
## lá na %AppData%, é melhor usar ela porque ela você tem certeza que você vai poder escrever,
## pois você geralmente não vai conseguir escrever no "res://", que é a pasta do jogo.

var save_path = "user://game.save"
var settings_path = "user://settings.save"
var paused = false

## SIGNALS ##

signal saved_game
signal quit_game

## MAIN MENU ##
var menu_background_list = []
const MENU_BACKGROUNDS_PATH = "res://assets/menu_backgrounds/"
var menu_current_background

## SETTINGS ##
var sound_volume = 50
var music_volume = 50

## PLAYER DATA ##

var player_name = "Euzinha"
var gus_intimacy = 2
var chapter = "Introduction"
var loading = false
var data = null

## FUNCTIONS ##

func _ready():
	load_settings()
	set_menu_background()

## Opens the directory with the background images and chooses a random one to be the background.
func set_menu_background():
	
	var directory = DirAccess.open(MENU_BACKGROUNDS_PATH)
	directory.list_dir_begin()
	
	while true:
		var file_name = directory.get_next()
		if file_name == "":
			break

		if file_name.ends_with(".remap"):
			file_name = file_name.replace(".remap", "")
		if file_name.ends_with(".import"):
			file_name = file_name.replace(".import", "")
		## Tirar o .import acaba duplicando as entradas de background no editor,
		## mas na hora de buildar o jogo ele só tem o .import, então fica certo.

		if !file_name.begins_with("."):
			
			menu_background_list.append(load(MENU_BACKGROUNDS_PATH + file_name))
	
	directory.list_dir_end()
	
	randomize()
	menu_current_background = menu_background_list[randi()%menu_background_list.size()]


## Pause the game
func pause_handler():
	if paused:
		paused = false
		get_tree().current_scene.PauseMenu.hide()
		#if Dialogic.current_timeline != null:
		Dialogic.paused = false
		Engine.time_scale = 1
	else:
		paused = true
		get_tree().current_scene.PauseMenu.show()
		#if Dialogic.current_timeline != null:
		Dialogic.paused = true
		Engine.time_scale = 0


## A QUESTÃO AQUI É QUE QUANDO VOLTAMOS PARA O MAIN MENU, PRECISAMOS RESETAR TODO O ESTADO DO DIALOGIC
## TALVEZ NÃO PRECISE DESSE KEEP_VARIABLES MAIS JÁ QUE ESTAMOS SINCRONIZANDO TUDO COM O GAMEINFO AO
## COMEÇAR UM NOVO CAPÍTULO (PARA SABER MAIS, VEJA O dialogic_scene.gd)

## Quit game
func quit_handler():
	saved_game.connect(_quit_)
	save_game()
	#print("Game saved")

func _quit_():
	quit_game.emit()
	saved_game.disconnect(_quit_)
	await pause_handler()
	#Dialogic.Clear._execute()
	#Dialogic.clear()
	Dialogic.end_timeline()
	Dialogic.clear(Dialogic.ClearFlags.KEEP_VARIABLES)
	get_tree().change_scene_to_file("res://main_menu/MainMenu.tscn")

func dizimate():

	var final_time = 0
	if Dialogic.has_subsystem("Text"):
		Dialogic.Text.update_dialog_text('')
		Dialogic.Text.hide_text_boxes()
		Dialogic.current_state = Dialogic.States.IDLE

	if Dialogic.has_subsystem('Portraits') and len(Dialogic.Portraits.get_joined_characters()) != 0:
		if final_time == 0:
			Dialogic.Portraits.leave_all_characters(DialogicUtil.guess_animation_file('Instant In Or Out'), final_time, true)
		else:
			Dialogic.Portraits.leave_all_characters("", final_time, true)

	if Dialogic.has_subsystem('Backgrounds') and Dialogic.Backgrounds.has_background():
		Dialogic.Backgrounds.update_background('', '', final_time)

	if Dialogic.has_subsystem('Audio') and Dialogic.Audio.has_music():
		Dialogic.Audio.update_music('', 0.0, "", final_time)

	if Dialogic.has_subsystem('Styles'):
		Dialogic.Styles.load_style()

	if Dialogic.has_subsystem('Portraits'):
		Dialogic.Portraits.reset_all_portrait_positions()

	Dialogic.clear()



## ISSO AQUI DEVERIA SER FÁCIL DE ENTENDER

## Save game settings
func save_settings():
	var file = FileAccess.open(settings_path, FileAccess.WRITE)
	file.store_var(sound_volume)
	file.store_var(music_volume)
	file.close()

## Save game data
func save_game():
	var file = FileAccess.open(save_path, FileAccess.WRITE)
	data = Dialogic.get_full_state()
	file.store_var(data)
	file.store_var(chapter)
	file.store_var(player_name)
	file.close()
	saved_game.emit()


## Load game settings
func load_settings():
	if !FileAccess.file_exists(settings_path):
		#print("Settings Save file not found")
		return
	var file = FileAccess.open(settings_path, FileAccess.READ)
	sound_volume = file.get_var()
	music_volume = file.get_var()
	file.close()

## Load game data
func load_game():
	loading = true
	if !FileAccess.file_exists(save_path):
		#print("Game Save file not found")
		loading = false
		return
	var file = FileAccess.open(save_path, FileAccess.READ)
	data = file.get_var()
	chapter = file.get_var()
	player_name = file.get_var()
	file.close()
	var path = "res://visual_novel/" + chapter + ".tscn"
	get_tree().change_scene_to_file(path)